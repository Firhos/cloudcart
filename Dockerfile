FROM maven:3.3.9-jdk-8

# Copy sources
RUN mkdir -p /srv

COPY pom.xml /srv/pom.xml
COPY src /srv/src

WORKDIR /srv

RUN mvn package

EXPOSE 8080

CMD mvn package tomee:run