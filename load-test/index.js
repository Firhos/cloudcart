const loadtest = require('loadtest');

const options = {
    url: 'http://51.75.13.25:30865/CloudCart/catalogue',
    concurrency: 50,
    maxRequests: 10000,
    maxSeconds: 300
};

console.log('Bench started...');
loadtest.loadTest(options, (error, result) => {
    if (error) {
        console.error(error);
        process.exit(1);
    }

    if (result.totalTimeSeconds > 60) {
        console.log('Load test failed.');
        process.exit(1);
    } else {
        console.log('Load test succeeded.');
        process.exit(0);
    }
});