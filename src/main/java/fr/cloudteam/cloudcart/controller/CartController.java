package fr.cloudteam.cloudcart.controller;

import fr.cloudteam.cloudcart.bean.PanierBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet(name = "CartController", value = "/cart")
public class CartController extends AbstractController {

  public static final String CART_VIEW = "/WEB-INF/jsp/cart.jsp";

  @Resource
  private DataSource cloudCartDataSource;

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setAttribute("isPayment", false);

    List<Integer> billID = new ArrayList<>();
    Connection dataBaseConnection = null;
    PanierBean panier = getOrCreatePanierFromSession(request);
    String selectBillsIdForCookieClient = "SELECT id FROM orders WHERE user_id = ?;";
    try {
      dataBaseConnection = this.cloudCartDataSource.getConnection();
      PreparedStatement preparedStatement = dataBaseConnection.prepareStatement(selectBillsIdForCookieClient);
      preparedStatement.setString(1, panier.getCookieClient());
      ResultSet rs = preparedStatement.executeQuery();

      while (rs.next()) {
        billID.add(rs.getInt(1));
      }
    }
    catch (final SQLException sqlException) {
      sqlException.printStackTrace();
    }
    finally {
      try {
        dataBaseConnection.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    request.setAttribute("billID", billID);
    doRedirect(request, response, CART_VIEW);
  }
}
