package fr.cloudteam.cloudcart.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import fr.cloudteam.cloudcart.bean.PanierBean;
import fr.cloudteam.cloudcart.pojo.Article;
import fr.cloudteam.cloudcart.service.DatabaseService;

@WebServlet(name = "PaymentController", value = "/payment")
public class PaymentController extends AbstractController {

  public static final String PAYMENT_VIEW = "/WEB-INF/jsp/payment.jsp";
  
  
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {


  }

  protected void doGet(HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {   
    request.setAttribute("isPayment", true);
    doRedirect(request, response, PAYMENT_VIEW);

  }
}
