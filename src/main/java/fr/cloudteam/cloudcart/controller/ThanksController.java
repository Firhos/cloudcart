package fr.cloudteam.cloudcart.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import fr.cloudteam.cloudcart.bean.PanierBean;
import fr.cloudteam.cloudcart.pojo.Article;

@WebServlet(name = "ThanksController", value = "/confirm")
public class ThanksController extends AbstractController{
	
	public static final String THANKS_VIEW = "/WEB-INF/jsp/thanks.jsp";

	@Resource
	private DataSource cloudCartDataSource;
	  
	@Override
	protected void doGet(HttpServletRequest request,
		      HttpServletResponse response)
		      throws ServletException, IOException {   
		  
		  Connection dataBaseConnection = null;
			PanierBean panier = this.getOrCreatePanierFromSession(request);
			String orderStatement = "INSERT INTO orders(user_id, date, total) VALUES(?, ?, ?) RETURNING id;";
		    String orderItemsStatement = "INSERT INTO order_items(order_id, product_id, quantity, unit_price) VALUES(?, ?, ?, ?);";
		    Date now = new Date();
		    double total = 0.0;
		    Map<Article, Integer> articlesAndQuantity = panier.getArticles();
		    
		    // On calcule le total du panier
		    for(Article article : articlesAndQuantity.keySet()) {
		    	total += article.getPrixUnite() * articlesAndQuantity.get(article);
		    }
		    try {
		        // Insertion de la commande
		    	dataBaseConnection = this.cloudCartDataSource.getConnection();
		    	PreparedStatement insertNewOrder = dataBaseConnection.prepareStatement(orderStatement); 
		    	insertNewOrder.setString(1, panier.getCookieClient());
		    	insertNewOrder.setDate(2, new java.sql.Date(now.getTime()));
		    	insertNewOrder.setDouble(3, total);
		    	ResultSet rs = insertNewOrder.executeQuery();
		    					  
		    	// Insertion des articles
		    	rs.next();
		    	int insertedId = rs.getInt(1);
		    	
		    	PreparedStatement insertAllProductsOrdered = dataBaseConnection.prepareStatement(orderItemsStatement);
		    	insertAllProductsOrdered.setLong(1, insertedId);
		    	
		        for(Article article : articlesAndQuantity.keySet()) {
		        	insertAllProductsOrdered.setString(2, article.getReference());
		        	insertAllProductsOrdered.setInt(3, articlesAndQuantity.get(article));
		        	insertAllProductsOrdered.setDouble(4, article.getPrixUnite());
		        	insertAllProductsOrdered.executeUpdate();
		        }

		    	
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}	  
		    
		    panier.getArticles().clear();
		    this.doRedirect(request, response, THANKS_VIEW);   
	
	}
}
