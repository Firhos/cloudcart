package fr.cloudteam.cloudcart.rest;

import static fr.cloudteam.cloudcart.rest.AddArticle.CART;

import fr.cloudteam.cloudcart.bean.PanierBean;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/download")
public class DownloadBill {

  @Context
  private HttpServletRequest request;

  @Resource
  private DataSource cloudCartDataSource;

  @GET
  @Path("/bill")
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  public Response download(@QueryParam(value = "billID") String billID) {

    String checkBillIDQuery = "SELECT total FROM orders WHERE user_id = ? AND id = ?;";
    String getBillInfos = "SELECT * FROM order_items WHERE order_id = ?;";
    Connection dataBaseConnection = null;
    PanierBean panierBean = (PanierBean)request.getSession().getAttribute(CART);

    if (panierBean == null)
      return Response.status(Status.BAD_REQUEST).entity("It seems you're no longer connected").build();

    try {
      dataBaseConnection = cloudCartDataSource.getConnection();
      PreparedStatement preparedStatement = dataBaseConnection.prepareStatement(checkBillIDQuery);
      preparedStatement.setString(1, panierBean.getCookieClient());
      preparedStatement.setInt(2, Integer.parseInt(billID));
      ResultSet rs = preparedStatement.executeQuery();

      if (!rs.next()) {
        return Response.status(Status.UNAUTHORIZED)
            .entity("Don't try to download bills of other users !").build();
      }
      float totalBill = rs.getFloat(1);

      preparedStatement = dataBaseConnection.prepareStatement(getBillInfos);
      preparedStatement.setInt(1, Integer.parseInt(billID));
      rs = preparedStatement.executeQuery();

      StringBuilder sb = new StringBuilder();
      sb.append("Facture n°" + billID + "\n\n");
      while(rs.next()) {
        sb.append("Ref produit: " + rs.getString(3) + ", prix: " + rs.getFloat(5) + ", quantité: " + rs.getInt(4) + "\n");
      }
      sb.append("\nTotal: " + totalBill);

      ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes());

      return Response.status(Status.OK).entity(bis).header("Content-Disposition", "attachment; filename=\"facture" + billID + ".txt\"").build();
    }
    catch (SQLException sqlException) {
      sqlException.printStackTrace();
    }

    return Response.status(Status.INTERNAL_SERVER_ERROR).build();
  }

}
