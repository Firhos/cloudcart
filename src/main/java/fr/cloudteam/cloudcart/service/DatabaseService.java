package fr.cloudteam.cloudcart.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseService {
	private static final DatabaseService instance = new DatabaseService();
	
	public long getInsertedId(Statement statement) throws SQLException {
		ResultSet rs = statement.getGeneratedKeys();
		
		if (rs.next()) {
		    return rs.getInt(1);
		}
		else {
			throw new SQLException("No ID provided");
		}
	}
	
	public static DatabaseService getInstance() {
		return instance;
	}
}
