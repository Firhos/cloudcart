<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ include file="parts/header.jsp" %>

<h1>Cart</h1>

<c:if test="${emptyCart}">
    <p><strong style="color: red;"> Votre panier est vide</strong></p>
</c:if>

<%@ include file="parts/tableCart.jsp" %>

<a href="/cloudcart/payment">Confirmer</a>

<h1>Mes factures</h1>

<ul>
    <c:forEach items="${billID}" var="id">
        <li><a href="/cloudcart/download/bill?billID=${id}" target="_blank">Facture n°${id}</a></li>
    </c:forEach>
</ul>

<%@ include file="parts/footer.jsp" %>