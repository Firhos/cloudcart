<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="parts/header.jsp" %>

<div id="thanks_container">
	<div id="green_tick">
		<img src="https://png2.kisspng.com/20180409/tfe/kisspng-fingerprint-comcast-circle-symbol-technology-tick-5acb37d74c0955.5506483815232675433115.png" class="">
	</div>
	<h1>Merci de votre confiance</h1>
	<p>
		Vous serez livré dans les meilleurs délais <br/>
		<a href="catalogue">Revenir au catalogue</a>
	</p>
</div>

<%@ include file="parts/footer.jsp" %>